#include<iostream>
#include"BigInteger.h"
#include<cassert>

std::ostream & operator << (std::ostream & _o, BigInteger &  _bigNumber)
{
	int * temp = _bigNumber.Inverse();
	for (int i = 0; i < _bigNumber.getNumDigits(); i++)
	{
		_o << temp[i];
	}
	return _o;
}
std::istream & operator >> (std::istream & _i, BigInteger & _bigNumber)
{
	char buf[100];
	_i >> buf;
	_bigNumber = BigInteger(buf);
	return _i;
}

void main ()
{

}