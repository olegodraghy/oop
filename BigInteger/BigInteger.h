#ifndef _BigInteger_H
#define _BigInteger_H
#include<string>


class BigInteger
{
public:
	BigInteger(int _maxSize);
	BigInteger(std::string _number);
	BigInteger(const BigInteger & _bigNumber);
	BigInteger(BigInteger && _bigNumber);
	BigInteger & operator = (BigInteger &_bigNumber);
	BigInteger & operator = (BigInteger && _bigNumber);
	~BigInteger();
	int getNumDigits() const;
	int operator [] (int _position) const;
	int & operator [] (int _position);
	char getDigit(int _position) const;
	void setDigit(int _position, char _digit);
	int * Inverse();
	bool operator == (BigInteger _bigNumber) const;
	bool operator != (BigInteger _bigNumber) const;
	bool operator < (BigInteger _bigNumber) const;
	bool operator <= (BigInteger _bigNumber) const;
	bool operator > (BigInteger _bigNumber) const;
	bool operator >= (BigInteger _bigNumber) const;
	operator bool () const;
	BigInteger & operator += (int _number);
	BigInteger & operator += (BigInteger _bigNumber);
	BigInteger  operator + (int _number);
	BigInteger  operator + (BigInteger _bigNumber);
private:
	int m_size;
	int * m_number;
	int * expandArray();
	void expandArray(int _size);
	int getTempLength(int _number);
	int * convertToArray(int _number);
};

inline int BigInteger::getNumDigits() const
{
	return m_size;
}

inline int * BigInteger::expandArray()
{
	int * tempNumber = new int[m_size + 1];
	for (int i = 0; i < m_size; i++)
	{
		tempNumber[i] = m_number[i];
	}
	return tempNumber;
}

inline void BigInteger::expandArray(int _size)
{
	int * tempNumber = new int[_size];
	for (int i = 0; i < m_size; i++)
	{
		tempNumber[i] = m_number[i];
	}
	delete[] m_number;
	m_number = tempNumber;
	m_size = _size;//new m_size
}

inline int BigInteger::getTempLength(int _number)
{
	int lengthOfINumber = 1;
	int temp = _number;
	while (_number /= 10)
	{
		lengthOfINumber++;
	}
	return lengthOfINumber;
}

inline int * BigInteger::convertToArray(int _number)
{
	int lengthOfINumber = getTempLength(_number);
	int temp = _number;
	int * tempInt = new int[lengthOfINumber];
	int k = 0;
	while (temp)
	{
		tempInt[k++] = temp % 10;
		temp /= 10;
	}
	return tempInt;
}
#endif