#include"BigInteger.h"
#include<algorithm>

BigInteger::BigInteger(int _maxSize)
{
	if (_maxSize <= 0)
		std::logic_error("Invalid size!");
	else
	{
		m_size = _maxSize;
		m_number = new int[_maxSize];
	}
}

BigInteger::BigInteger(std::string _number)
{
	if (std::all_of(_number.begin(), _number.end(), ::isdigit))
	{
		m_size = _number.length();
		m_number = new int[_number.length()];
		for (int i = 0; i < m_size; i++)
		{
			m_number[m_size - i - 1] = _number.at(i) - '0';
		}
	}
	else
	{
		std::logic_error("Not a number");
	}
}

BigInteger::BigInteger(const BigInteger & _bigNumber)
{
	m_size = _bigNumber.m_size;
	m_number = new int[m_size];
	for (int i = 0;i < m_size;i++)
	{
		m_number[i] = _bigNumber.m_number[i];
	}
}

BigInteger::BigInteger(BigInteger && _bigNumber)
{
	m_size = _bigNumber.m_size;
	m_number = _bigNumber.m_number;
	_bigNumber.m_number = nullptr;
}

BigInteger & BigInteger::operator = (BigInteger & _bigNumber)
{
	if (this == & _bigNumber)
	{
		return *this;
	}
	delete[] m_number;
	m_size = _bigNumber.m_size;
	m_number = new int[m_size];
	for (int i = 0; i < m_size; i++)
	{
		m_number[i] = _bigNumber.m_number[i];
	}
	return *this;
}

BigInteger & BigInteger::operator = (BigInteger && _bigNumber)
{
	if (this == &_bigNumber)
	{
		return *this;
	}
	int tempSize = _bigNumber.m_size;
	int * tempNumber = _bigNumber.m_number;
	_bigNumber.m_size = m_size;
	_bigNumber.m_number = m_number;
	m_size = tempSize;
	m_number = tempNumber;
	return *this;
}

BigInteger::~BigInteger()
{
	delete[] m_number;
}

bool BigInteger::operator==(BigInteger _bigNumber) const
{
	if (m_size != _bigNumber.m_size)
	{
		return false;
	}
	else
	{
		for (int i = 0;i < m_size;i++)
		{
			if (m_number[i] != _bigNumber.m_number[i])
				return false;
		}
		return true;
	}
}

bool BigInteger::operator!=(BigInteger _bigNumber) const
{
	return !(*this == _bigNumber);
}

bool BigInteger::operator < (BigInteger _bigNumber) const
{
	if (m_size < _bigNumber.m_size)
		return true;
	else
	{
		if (*this == _bigNumber)
			return false;
		else
		{
			for (int i = m_size - 1;i >= 0;i--)
			{
				if (m_number[i] < _bigNumber.m_number[i])
					return true;
				else if (m_number[i] == _bigNumber.m_number[i])
				{
					continue;
				}
				else
				{
					return false;
				}
			}
		}
	}
	return false;
}

bool BigInteger::operator<=(BigInteger _bigNumber) const
{
	return (*this < _bigNumber || *this == _bigNumber);
}

bool BigInteger::operator>=(BigInteger _bigNumber) const
{
	return !(*this <= _bigNumber);
}

bool BigInteger::operator>(BigInteger _bigNumber) const
{
	return _bigNumber < *this;
}

char BigInteger::getDigit(int _position) const
{
	return (char)m_number[_position];
}

int BigInteger::operator [] (int _position) const
{
	return m_number[_position];
}

int & BigInteger::operator [] (int _position)
{
	return m_number[_position];
}

BigInteger BigInteger::operator+(int _number)
{
	BigInteger temp = *this;
	temp += _number;
	return temp;
}

BigInteger BigInteger::operator+(BigInteger  _bigNumber)
{
	BigInteger temp = *this;
	temp += _bigNumber;
	return temp;
}

BigInteger & BigInteger::operator+=(int _number)
{
	if (_number == 0)
		return * this;
	int lengthOfINumber = getTempLength(_number);
	int * tempInt = convertToArray(_number);
	expandArray(lengthOfINumber);
	for (int i = 0; i < lengthOfINumber; i++)
	{
		if (m_number[i] + tempInt[i] >= 10)
		{
			if (i == lengthOfINumber - 1)//if last digit - need to add a new element in BigInteger's array
			{
				int * tempNumber = expandArray();
				delete[] m_number;
				m_number = new int[m_size + 1];
				m_size++;
				for (int i = 0; i < m_size; i++)//copy
				{
					m_number[i] = tempNumber[i];
				}
				m_number[m_size - 1] = 0;//adding new bit to BigInteger
			}
			m_number[i + 1]++;
			m_number[i] = (m_number[i] + tempInt[i]) % 10;
		}
		else
		{
			m_number[i] += tempInt[i];
		}
	}
	return * this;
}

BigInteger & BigInteger::operator+=(BigInteger _bigNumber)
{
	int lessInteger;
	if (m_size >= _bigNumber.m_size)
	{
		lessInteger = _bigNumber.m_size;
	}
	else
	{
		lessInteger = m_size;
	}
	for (int i = 0; i < lessInteger; i++)
	{
		if (m_number[i] + _bigNumber.m_number[i] >= 10)
		{
			m_number[i + 1]++;
			m_number[i] = (m_number[i] + _bigNumber.m_number[i]) % 10;
		}
		else
		{
			m_number[i] += _bigNumber.m_number[i];
		}
	}
	return * this;
}

void BigInteger::setDigit(int _position, char _digit)
{
	m_number[_position] = (int)_digit;
}

BigInteger::operator bool() const
{
	for (int i = 0; i < m_size; i++)
	{
		if (m_number[i] == 0)
			if (i == m_size - 1)
			{
				return false;
			}
			else
			{
				continue;
			}
		else
			return true;
	}
}

int * BigInteger::Inverse()
{
	int * temp = new int[m_size];
	for (int i = 0; i < m_size; i++)
	{
		temp[i] = m_number[m_size - i - 1];
	}
	return temp;
}